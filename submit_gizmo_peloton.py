#!/usr/bin/env python3

#SBATCH --job-name=gizmo
#SBATCH --partition=high2    # peloton node: 32 cores, 7.8 GB per core, 250 GB total
##SBATCH --partition=high2m    # peloton high-mem node: 32 cores, 15.6 GB per core, 500 GB total
#SBATCH --mem=125G
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=32    # MPI tasks per node
#SBATCH --cpus-per-task=1    # OpenMP threads per MPI task
#SBATCH --time=240:00:00
#SBATCH --output=gizmo_jobs/gizmo_job_%j.txt
#SBATCH --mail-user=XXXXl@gmail.com
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --mail-type=begin

'''
Submit a Gizmo simulation jobs to the SLURM queue.
Check if restart files exist - if yes, start a restart run.

@author: Andrew Wetzel <arwetzel@gmail.com>
'''

import os
# local ----
from utilities.basic import io as ut_io

# whether restart should be from snapshot file
restart_from_snapshot = False

# names of files and directories
executable_file_name = './gizmo/GIZMO'  # executable file
parameter_file_name = 'gizmo_parameters.txt'  # parameter file

# set and print compute parameters
SubmissionScript = ut_io.SubmissionScriptClass('slurm')

# check if input id of previous job that needs to finish before this starts
SubmissionScript.check_existing_job('submit_gizmo_stampede.py', submit_before=False, time_delay=4)

# set execution command
execute_command = 'ibrun mem_affinity {} {}'.format(executable_file_name, parameter_file_name)
# check if restart
execute_command += SubmissionScript.get_restart_flag(restart_from_snapshot)
# set output files
execute_command += ' 1> gizmo.out 2> gizmo.err'
print('executing: {}\n'.format(execute_command))
os.sys.stdout.flush()

# execute
os.system(execute_command)

SubmissionScript.print_runtime()
