#!/usr/bin/env python3

#SBATCH --job-name=hello_world
#SBATCH --partition=high2    # peloton node: 32 cores, 7.8 GB per core, 250 GB total
##SBATCH --partition=high2m    # peloton high-mem node: 32 cores, 15.6 GB per core, 500 GB total
#SBATCH --mem=125G
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=32    # MPI tasks per node
#SBATCH --cpus-per-task=1    # OpenMP threads per MPI task
#SBATCH --time=00:05:00
#SBATCH --output=/home/ibsantis/scripts/job_reports/hello_world_%j.txt
#SBATCH --mail-user=ibsantistevan@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --mail-type=begin

'''
Testing something
'''

import os
# local ----
from utilities.basic import io as ut_io

# print run-time and CPU information
ScriptPrint = ut_io.SubmissionScriptClass('slurm')
os.system('python /home/ibsantis/scripts/sim_testing/peloton_slurm.py')
# print run-time information
ScriptPrint.print_runtime()
